#!/bin/sh

#my_num_days=5
#my_begin_snap=138779
#my_end_snap=138815
#my_int=2
#my_report_basename=report
my_report_type=html
my_report_dir=report

# env settings if needed to change
ORACLE_SID=mpsbase
ORACLE_HOME=/opt/oracle/product/ora11203

my_set_var() {
 tmp_name=${1}.tmp
 sed -e "s/^\( *define *${2} *=\).*/\1 ${3};/" ${1} > ${tmp_name}
 mv ${tmp_name} ${1}
}

clear

echo "Enter value for num days: "
read my_num_days
# write num_days into prameter file
my_set_var params.sql num_days $my_num_days

# generating list of snaps
# set dummy to begin and end snaps
my_set_var params.sql begin_snap 0
my_set_var params.sql end_snap 0
${ORACLE_HOME}/bin/sqlplus / as sysdba @params.sql

echo
echo "-------------------------------------------------------------------------------------------"
echo " ATTENTION! DO NOT WORRY ABOUT ERRORS ABOVE. IF YOU SEE THE LIST OF SNAPS EVERYTHING IS OK "
echo "-------------------------------------------------------------------------------------------"
echo
# end of generating list of snaps

echo "Enter value for begin snap: "
read my_begin_snap
echo "Enter value for end snap: "
read my_end_snap
echo "Enter value for interval between snaps: "
read my_int
echo "Enter value for report base name: "
read my_report_basename

# write report_type into prameter file
my_set_var params.sql report_type $my_report_type


my_current_report=0
while [ $my_begin_snap -lt $my_end_snap ]
do
	my_set_var params.sql begin_snap $my_begin_snap
	my_begin_snap=$(expr ${my_begin_snap} + ${my_int})
	my_set_var params.sql end_snap $my_begin_snap
	my_report_name="${my_report_dir}\/${my_report_basename}_${my_current_report}.${my_report_type}"
	my_set_var params.sql report_name $my_report_name
	my_current_report=$(expr ${my_current_report} + 1)

	${ORACLE_HOME}/bin/sqlplus / as sysdba @params.sql
done
